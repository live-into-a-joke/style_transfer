import uuid
from datetime import datetime
import base64
import json
import requests
from flask import Flask, request, jsonify, send_file, Response, redirect
from io import BytesIO
from PIL import Image
from flask_cors import CORS
import os
import pymysql

def insert_data(photo_id, expression, age, level, gender, race):
    conn = pymysql.connect(
        host='127.0.0.1',
        user='root',
        passwd='zy..1**2',
        port=3306,
        db='zg_dev',
        charset='utf8'
    )
    cur = conn.cursor()
    photo_id=photo_id.replace(".jpg", "")
    current_datetime = datetime.now()
    formatted_datetime = current_datetime.strftime("%Y-%m-%d %H:%M:%S")

    sql = f"INSERT INTO zg_face (photo_id, expression, age, level, gender, race, date) VALUES ('{photo_id}', '{expression}', {age}, {level}, '{gender}', '{race}', '{formatted_datetime}')"
    cur.execute(sql)

    conn.commit()
    cur.close()
    conn.close()


class BaiduAI:
    def __init__(self, img):
        self.img=img
        print(self.img)
        self.AK = "OU9hIN3MyKpHE0mTve1IpTOG"
        self.SK = "KRljrcR1gwoX2E9fGW5K7h5U6jLG4eO5"
        self.img_src = "input/"+img
        print(self.img_src)
        self.headers = {
            "Content-Type": "application/json; charset=UTF-8"
        }

    def get_AccessToken(self):
        #获取Access Token
        host = 'https://aip.baidubce.com/oauth/2.0/token?grant_type=client_credentials&client_id=' + self.AK + '&client_secret=' + self.SK
        response = requests.get(host, headers=self.headers)
        json_result = json.loads(response.text)
        if response:
            return json_result['access_token']
        else:
            print(json_result)
            return 0

    def img_to_base64(slef, path):
        #图片转化为base64
        with open(path, 'rb') as f:
            image = f.read()
            image_base64 = str(base64.b64encode(image), encoding='utf-8')
        return image_base64

    def face_identification(self):
        # 人脸检测与属性分析
        img = self.img_to_base64(self.img_src)
        request_url = "https://aip.baidubce.com/rest/2.0/face/v3/detect"
        post_data = {
            "image": img,
            "image_type": "BASE64",
            "face_field": "gender,age,beauty,gender,race,emotion,face_shape,landmark",#包括age,beauty,expression,face_shape,gender,glasses,landmark,emotion,face_type,mask,spoofing信息
            "face_type": "LIVE"#人脸的类型。LIVE表示生活照，IDCARD表示身份证芯片照，WATERMARK表示带水印证件照，CERT表示证件照片，默认LIVE。
        }
        access_token = self.get_AccessToken()
        request_url = request_url + "?access_token=" + access_token
        response = requests.post(url=request_url, data=post_data, headers=self.headers)
        json_result = json.loads(response.text)
        print(json_result)
        if json_result['error_code'] == 0:
            print("人脸表情：", json_result['result']['face_list'][0]['emotion']['type'])
            print("人物年龄：", json_result['result']['face_list'][0]['age'])
            print("人物颜值评分：", json_result['result']['face_list'][0]['beauty'])
            print("人物性别：", json_result['result']['face_list'][0]['gender']['type'])
            print("人物种族：", json_result['result']['face_list'][0]['race']['type'])
            #print("人物特征点位置：", json_result['result']['face_list'][0]['landmark72'])

            photo_id = self.img
            expression = json_result['result']['face_list'][0]['emotion']['type']
            age = json_result['result']['face_list'][0]['age']
            level = json_result['result']['face_list'][0]['beauty']
            gender = json_result['result']['face_list'][0]['gender']['type']
            race = json_result['result']['face_list'][0]['race']['type']

            insert_data(photo_id, expression, age, level, gender, race)

        else:
            print(json_result['error_code'])
            print(json_result['error_msg'])


def save_photo_to_input_folder(photo_name, input_image):
    # 指定目标文件夹路径
    target_folder = "input"
    # 确保目标文件夹存在
    if not os.path.exists(target_folder):
        os.mkdir(target_folder)
    # 构建完整的保存路径
    save_path = os.path.join(target_folder, f"{photo_name}.jpg")
    # 保存照片到指定路径
    input_image.save(save_path)

#定义全局变量
alt_text = ""

app = Flask(__name__)
CORS(app)  # 添加CORS头信息


@app.route('/style_transfer.html')
def show_html():
    return send_file('style_transfer.html')

@app.route('/get_photos')
def get_photos():
    photo_dir = 'resource'
    photo_names = os.listdir(photo_dir)
    return jsonify({'photoNames': photo_names})

@app.route('/process_photo', methods=['POST'])
def process_photo():
    global alt_text
    print("old alt_text:{}".format(alt_text))
    alt_text = request.form['altText']
    # 在这里进行相应的处理逻辑
    print("new alt_text:{}".format(alt_text))
    print(alt_text.split("."))
    return redirect('/style_transfer.html')

@app.route('/style_transfer', methods=['POST'])
def do_style_transfer():
    content_image = request.files.get('content_image')
    global alt_text
    Alist=alt_text.split(".")
    model_style=Alist[0]
    style_id=Alist[1]
    image_data = content_image.read()

    input_image = Image.open(BytesIO(image_data))
    if input_image.mode == 'RGBA':
        input_image = input_image.convert('RGB')
    photo_name = str(uuid.uuid4())
    save_photo_to_input_folder(photo_name,input_image)
    # 调用你的风格迁移算法进行处理
    loc="./style_transfer.py"
    #output_image = os.system('python {} --content {} --scale_image --style_id {} --style_degree 0.5 --ckpt ../checkpoint/vtoonify_d_cartoon/vtoonify_s_d.pt --cpu --padding 600 600 600 600'.format(loc,save_input_path,alpha))
    os.system("python style_transfer.py --content ./input/{}.jpg \
       --scale_image --style_id {} --style_degree 0.5 \
       --ckpt checkpoint/{}/vtoonify_s_d.pt \
       --cpu --padding 600 600 600 600     # use large padding to avoid cropping the image".format(photo_name,style_id,model_style))
    # 返回处理结果
    demo = BaiduAI(r'{}.jpg'.format(photo_name))
    if (demo.get_AccessToken()):
        demo.face_identification()
    response = jsonify({'output_image': r'{}_vtoonify_d.jpg'.format(photo_name)})
    response.headers.add('Access-Control-Allow-Origin', '*') # 允许跨域访问
    return response

@app.route('/api/beauty_ranking', methods=['GET'])
def get_beauty_ranking():
    conn = pymysql.connect(
        host='127.0.0.1',
        user='root',
        passwd='zy..1**2',
        port=3306,
        db='zg_dev',
        charset='utf8'
    )
    cur = conn.cursor()

    # 查询颜值排名数据
    sql = "SELECT * FROM zg_face ORDER BY level desc"
    cur.execute(sql)
    results = cur.fetchall()

    columns = [desc[0] for desc in cur.description]  # 获取列名
    # 构造响应数据
    ranking_data = []
    i = 1
    for row in results:
        photo_id = row[columns.index('photo_id')]
        expression = row[columns.index('expression')]
        age = row[columns.index('age')]
        level = row[columns.index('level')]
        gender = row[columns.index('gender')]
        race = row[columns.index('race')]
        date = row[columns.index('date')]
        ranking_data.append({
            'photo_id': photo_id,
            'expression': expression,
            'age': age,
            'level': level,
            'gender': gender,
            'race': race,
            'ranking': i,
            'date': date
        })
        i += 1

    cur.close()
    conn.close()
    # 返回JSON格式的数据
    return jsonify(ranking_data)

@app.route('/api/timing', methods=['GET'])
def get_timing():
    conn = pymysql.connect(
        host='127.0.0.1',
        user='root',
        passwd='zy..1**2',
        port=3306,
        db='zg_dev',
        charset='utf8'
    )
    cur = conn.cursor()

    # 查询颜值排名数据
    sql = "SELECT * FROM zg_face ORDER BY level desc"
    cur.execute(sql)
    results = cur.fetchall()

    columns = [desc[0] for desc in cur.description]  # 获取列名
    # 构造响应数据
    ranking_data = []
    i = 1
    for row in results:
        photo_id = row[columns.index('photo_id')]
        expression = row[columns.index('expression')]
        age = row[columns.index('age')]
        level = row[columns.index('level')]
        gender = row[columns.index('gender')]
        race = row[columns.index('race')]
        date = row[columns.index('date')]
        ranking_data.append({
            'photo_id': photo_id,
            'expression': expression,
            'age': age,
            'level': level,
            'gender': gender,
            'race': race,
            'ranking': i,
            'date': date
        })
        i += 1

    # 按照日期时间与当前时间的差距进行排序
    sorted_data = sorted(ranking_data, key=lambda x: x['date'], reverse=True)
    cur.close()
    conn.close()
    # 返回JSON格式的数据
    return jsonify(sorted_data)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=True) # 运行 Web 服务器
